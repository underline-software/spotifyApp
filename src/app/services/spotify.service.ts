import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {
  constructor(private http: HttpClient) {
  }

  public getQuery(qry: string){
    const url = `https://api.spotify.com/v1/${qry}`;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQAoc0QfzT8cuhfMVwZ1UhZ9IJksu9D4oJUyR-RSprXB87P6LcTUeGSCyf2_vcTJW0NaTzBcHRdTbyF5J3w'
    });
    return this.http.get(url,{headers});
  }

  public getNewReleases() {
    // const headers = new HttpHeaders({
    //   'Authorization': 'Bearer BQAIp8uOs65V5GVnNh5QWYEXqcXX-hOEDOqjH0rwL_Peuoaj3xTzgtB6fYkBB0mJOoS4bREvkEq_vmUqHcw'
    // });
    return this.getQuery('browse/new-releases?limit=21&country=CL')
      .pipe(map((data:any) => data.albums.items));

    // return this.http.get('https://api.spotify.com/v1/browse/new-releases?limit=21&country=CL',
    //   {
    //     headers
    //   }).pipe(map(
    //   (data:any) => data.albums.items));
  }

  public getArtistas(nameArtista: string){
    // const headers = new HttpHeaders({
    //   'Authorization': 'Bearer BQAIp8uOs65V5GVnNh5QWYEXqcXX-hOEDOqjH0rwL_Peuoaj3xTzgtB6fYkBB0mJOoS4bREvkEq_vmUqHcw',
    // });

    return this.getQuery(`search?q=${nameArtista}&type=artist&market=CL&limit=21`).pipe(map(
      (data:any) => data.artists.items));

    // return this.http.get(`https://api.spotify.com/v1/search?q=${nameArtista}&type=artist&market=CL&limit=21`,
    //   {
    //     headers
    //   }).pipe(map(
    //   (data:any) => data.artists.items));
  }

  public getArtista(idArtista: string){
    return this.getQuery(`artists/${idArtista}`);
      // .pipe(map((data:any) => data.artists.items));
  }

  public getTopTracks(idArtista: string){
    return this.getQuery(`artists/${idArtista}/top-tracks?country=es`)
      .pipe(map((data:any) => data.tracks));
  }
}
