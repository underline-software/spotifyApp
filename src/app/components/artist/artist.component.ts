import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {SpotifyService} from '../../services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styles: []
})
export class ArtistComponent implements OnInit {

  private artista: any = {};
  public loading_artist: boolean;
  public topTracks: any[] = [];

  constructor( private activatedRoute: ActivatedRoute,
               private spotifyService: SpotifyService) {
    this.activatedRoute.params.subscribe(params => {
     this.getArtista(params['id']);
     this.getTopTracks(params['id']);
    });
  }

  ngOnInit() {}

  public getArtista(id: string) {
    this.loading_artist = true;
    this.spotifyService.getArtista(id)
      .subscribe((artista: any) => {
        this.artista = artista;
        this.loading_artist = false;
      });
  }

  public getTopTracks(idArtista) {
    this.spotifyService.getTopTracks(idArtista)
      .subscribe( topTracks => {
        this.topTracks = topTracks;
      });
  }
}
