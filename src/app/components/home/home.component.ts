import {Component, Input, OnInit} from '@angular/core';
import {SpotifyService} from '../../services/spotify.service';

// import {HttpClient} from '@angular/common/http';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent {

  // paises: any[] = [];

  // constructor(private http: HttpClient) {
    // this.http.get('https://restcountries.eu/rest/v2/lang/es')
    //   .subscribe((response:any) => {
    //     this.paises = response;
    //     console.log(response);
    //   });  }

  private nuevasCanciones: any[] = [];
  private loading: boolean;
  private error: boolean;

  constructor(private spotify: SpotifyService) {
    this.loading = true;
    this.error = false;
    this.spotify.getNewReleases().subscribe(
      (data: any) => {
      this.nuevasCanciones = data;
      this.loading = false;
    },( errorServicio ) =>{
        this.loading = false;
        this.error = errorServicio.error.error.message;
      });
  }
}
