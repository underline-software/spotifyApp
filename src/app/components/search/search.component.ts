import { Component } from '@angular/core';
import {SpotifyService} from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent {
  public artistasEncontrados: any[] = [];
  public loading: boolean;
  constructor(private spotify: SpotifyService) {}

  public buscar(nameCancion: string) {
    this.loading = true;
    this.spotify.getArtistas(nameCancion)
      .subscribe((artists: any) => {
        this.artistasEncontrados = artists;
        this.loading = false;
      });
    // this.loading = false;
  }
}
