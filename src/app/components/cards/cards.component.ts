import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {

  @Input() canciones: any [] = [];
  constructor( private router: Router) {}

  ngOnInit() {
  }

  public verArtista(cancion: any) {
    let artistaId;
    if (cancion.type === 'artist') {
      artistaId = cancion.id;
    } else {
      artistaId = cancion.artists[0].id;
    }
    this.router.navigate(['artist', artistaId]);
  }

}
